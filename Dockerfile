FROM golang:latest

ENV PATH="/usr/local/go/bin:/root/go/bin:/opt/dprint/bin:${PATH}"

COPY dprint.json /dprint.json

RUN apt-get update \
    && apt-get install -y yapf3 curl unzip wget \
    && curl -fsSL https://dprint.dev/install.sh | DPRINT_INSTALL=/opt/dprint sh \
    && go install golang.org/x/tools/cmd/goimports@latest \
    && go install github.com/segmentio/golines@latest \
    && go install mvdan.cc/sh/v3/cmd/gosh@latest \
    && curl https://sh.rustup.rs -sSf | sh -s -- -y -q -c rustfmt

CMD ["bash"]
