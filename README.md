# dprint ci

Reusable GitLab CI jobs for [dprint](https://github.com/dprint/dprint). The template is in [dprint.gitlab-ci.yml](./dprint.gitlab-ci.yml)
see [.gitlab-ci.yml](./.gitlab-ci.yml) which uses it internally
